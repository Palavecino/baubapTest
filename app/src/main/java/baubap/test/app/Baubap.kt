package baubap.test.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Baubap  : Application() {

    override fun onCreate() {
        super.onCreate()
    }
}