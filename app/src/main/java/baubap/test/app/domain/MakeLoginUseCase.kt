package baubap.test.app.domain

import baubap.test.app.models.User
import baubap.test.app.repositories.LoginRepository
import javax.inject.Inject

class MakeLoginUseCase @Inject constructor(
    private val repository: LoginRepository
) {

    operator fun invoke(email: String?, password: String?): User? {
        return repository.makeLogin(email, password)
    }
}