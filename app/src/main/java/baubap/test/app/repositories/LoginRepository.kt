package baubap.test.app.repositories

import baubap.test.app.models.User
import baubap.test.app.utils.Persistence
import javax.inject.Inject

class LoginRepository @Inject constructor() {

    fun makeLogin(email: String?, password: String?): User? {
        return if (checkEmailAndPasswordAreValid(email, password)) {
            User(email, password)
        } else null
    }

    private fun checkEmailAndPasswordAreValid(email: String?, password: String?): Boolean {
        return (email.toString() == Persistence.email && password.toString() == Persistence.password)
    }
}