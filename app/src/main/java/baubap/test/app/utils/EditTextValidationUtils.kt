package baubap.test.app.utils

import java.util.regex.Pattern

class EditTextValidationUtils {
    companion object{

        fun checkIfIsEmail(text: String): Boolean {
            val expression = "^[\\w\\.+-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(text)
            return matcher.matches()
        }
    }
}