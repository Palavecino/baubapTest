package baubap.test.app.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import baubap.test.app.domain.MakeLoginUseCase
import baubap.test.app.models.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val makeLoginUseCase: MakeLoginUseCase
) : ViewModel() {
    val overlayLiveData = MutableLiveData(false)
    var email: MutableLiveData<String> = MutableLiveData()
    var password: MutableLiveData<String> = MutableLiveData()
    val successLogin: MutableLiveData<Boolean> = MutableLiveData()
    var failLogin: MutableLiveData<Boolean> = MutableLiveData()
    var user: MutableLiveData<User> = MutableLiveData()

    fun makeLogin() {
        overlayLiveData.postValue(true)
        sendRequest()
    }

    private fun sendRequest() {
        overlayLiveData.postValue(false)
        val user = makeLoginUseCase(email.value, password.value)
        this.user.postValue(user)
        if (user != null) {
            successLogin()
        } else {
            failLogin()
        }
    }

    private fun failLogin() {
        failLogin.postValue(true)
    }


    private fun successLogin() {
        successLogin.postValue(true)
    }

}