package baubap.test.app.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import baubap.test.app.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}