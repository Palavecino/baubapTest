package baubap.test.app.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import baubap.test.app.R
import baubap.test.app.databinding.ActivityLoginBinding
import baubap.test.app.utils.EditTextValidationUtils
import baubap.test.app.viewModels.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val viewModel: LoginViewModel by lazy { ViewModelProvider(this)[LoginViewModel::class.java] }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.loginViewModel = viewModel
        setupLoginButton()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.overlayLiveData.observe(this) { visible ->
            if (visible) binding.overlayContainer.visibility =
                View.VISIBLE else binding.overlayContainer.visibility = View.GONE
        }

        viewModel.successLogin.observe(this) { success ->
            Toast.makeText(this, getString(R.string.login_success), Toast.LENGTH_SHORT).show()
            goToHomeActivity()
        }

        viewModel.failLogin.observe(this) { fail ->
            Toast.makeText(this, getString(R.string.login_fail), Toast.LENGTH_SHORT).show()
        }
    }

    private fun goToHomeActivity() {
        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    private fun setupLoginButton() {
        binding.loginButton.setOnClickListener {
            if (areValidCredentials()) {
                makeLogin()
            }
        }
    }

    fun areValidCredentials(): Boolean {
        if (!EditTextValidationUtils.checkIfIsEmail(viewModel.email.value.toString())) {
            binding.emailText.requestFocus()
            binding.emailText.error = getString(R.string.enter_email)
            return false
        }
        if (viewModel.password.value.isNullOrEmpty()) {
            binding.passwordText.requestFocus()
            binding.passwordText.error = getString(R.string.enter_password)
            return false
        }
        return true
    }


    private fun makeLogin() {
        viewModel.makeLogin()
    }

}