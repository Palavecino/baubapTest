package baubap.test.app.domain

import baubap.test.app.models.User
import baubap.test.app.repositories.LoginRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any


class MakeLoginUseCaseTest{
    @RelaxedMockK
    private lateinit var loginRepository: LoginRepository

    lateinit var makeLoginUseCase: MakeLoginUseCase

    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        makeLoginUseCase = MakeLoginUseCase(loginRepository)
    }

    @Test
    fun `when the api returns a null user`(){
        //given
        every { loginRepository.makeLogin(any(), any()) } returns null

        //when
        val result = makeLoginUseCase(any(),any())

        //then
        assert(result == null)

    }


    @Test
    fun `when the api returns an user`(){
        //given
        val email = "lucas@mail.com"
        val password = "1234"
        val user = User(email, password)

        every { loginRepository.makeLogin(email, password) } returns user

        //when
        val result = makeLoginUseCase(email, password)

        //then
        assert(user == result)
    }
}