package baubap.test.app.viewModels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import baubap.test.app.domain.MakeLoginUseCase
import baubap.test.app.models.User
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.any


internal class LoginViewModelTest {
    @RelaxedMockK
    private lateinit var makeLoginUseCase: MakeLoginUseCase

    private lateinit var loginViewModel: LoginViewModel

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        loginViewModel = LoginViewModel(makeLoginUseCase)
    }


    @Test
    fun `show error message when return null user`() {
        //given
        val user = null
        coEvery { makeLoginUseCase(any(), any()) } returns user

        //when
        loginViewModel.makeLogin()

        //then
        assert(loginViewModel.user.value == null)
        assert(loginViewModel.failLogin.value == true)

    }

    @Test
    fun `show error message when return a valid user`() {
        //given
        val user = User(any(), any())

        every { makeLoginUseCase(any(), any()) } returns user

        //when
        loginViewModel.makeLogin()

        //then
        assert(loginViewModel.successLogin.value == true)
        assert(loginViewModel.user.value == user)
    }

}